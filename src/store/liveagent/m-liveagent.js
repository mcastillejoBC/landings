/* eslint-disable no-underscore-dangle */

const SET_ONLINE = 'SET_ONLINE';

const buttonId = '57309000000kckn';

export default {
	namespaced: true,

	state() {
		return { online: null };
	},

	mutations: {
		[SET_ONLINE](state, value) {
			state.online = value;
		},
	},

	actions: {
		/**
		 * Añadimos un indicador para gestionar eventos del liveagent button
		 * Añadimos deployment script para instalar liveagent app
		 * Iniciamos chat user session
		 */
		install({ dispatch }) {
			const deployment = 'c.la3-c2-fra';

			/* istanbul ignore else */
			if (!window.liveagent) {
				const script = document.createElement('script');
				script.dataset.liveagent = '';
				script.type = 'text/javascript';
				script.src = `https://${deployment}.salesforceliveagent.com/content/g/js/51.0/deployment.js`;
				script.onload = () => dispatch('init');

				document.body.append(script);
			}
		},

		init({ dispatch }) {
			const deploymentId = '57209000000kc41';
			const orgId = '00D09000007T9DK';
			const chatURL = 'https://d.la3-c2-fra.salesforceliveagent.com/chat';
			const onlineButtonId = `liveagent_button_online_${buttonId}`;
			const offlineButtonId = `liveagent_button_offline_${buttonId}`;

			/* istanbul ignore else */
			if (!window._laq) {
				window._laq = [];
			}

			/* istanbul ignore else */
			if (window.liveagent) {
				window._laq.push(() => {
					window.liveagent.showWhenOnline(buttonId, document.getElementById(onlineButtonId));
					window.liveagent.showWhenOffline(buttonId, document.getElementById(offlineButtonId));
				});

				window.liveagent.addButtonEventHandler(buttonId, (event) =>
					dispatch('eventHandler', event)
				);

				window.liveagent.init(chatURL, deploymentId, orgId);
			}
		},

		start() {
			window.liveagent.startChat(buttonId);
		},

		/**
		 * La app liveagent realiza un ping cada 5 segundos,
		 * asignamos valor al state cada vez que se verifique
		 * la disponibilidad del agente
		 */
		eventHandler({ commit, state }, event) {
			/**
			 * Los posibles eventos a recibir son:
			 * BUTTON_ACCEPTED - Cuando el usuario inicia chat
			 * BUTTON_AVAILABLE - Cuando se verifica disponibilidad del agente
			 * BUTTON_REJECTED - Cuando se rechaza un inicio de chat
			 * BUTTON_UNAVAILABLE - Cuando se verifica disponibilidad del agente
			 */
			const isOnline = event === 'BUTTON_ACCEPTED' || event === 'BUTTON_AVAILABLE';

			/* istanbul ignore else */
			if (state.online !== isOnline) {
				commit('SET_ONLINE', isOnline);
			}
		},
	},
};
