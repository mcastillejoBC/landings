import Vue from 'vue';
import Vuex from 'vuex';
import liveagent from './liveagent/m-liveagent';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: { liveagent },
	strict: process.env.NODE_ENV !== 'production',
});
